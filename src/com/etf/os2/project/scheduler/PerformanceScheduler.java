package com.etf.os2.project.scheduler;

import com.etf.os2.project.Main;
import com.etf.os2.project.process.Pcb;
import com.etf.os2.project.process.PcbData;
import com.etf.os2.project.process.Process;
import sun.misc.Perf;
import sun.reflect.generics.tree.Tree;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Filip Tanic on 8.1.2019..
 */

/** Has a fixed number of priorities of 5. It could be an issue, consider implementing a dynamic solution!
 *
 */

public class PerformanceScheduler extends Scheduler {

    private static int numCpus;
    private int numPrio = 7;//EDITABLE FACTOR

    private long initialStatistics[][];
    //EXPIRATION MECHANISM
    private PriorityQueue<Pcb> maxWaitQueue[];
    private PriorityQueue<Pcb> pool[][][];
    private int poolInUse = 0;
    private int poolExpired = 1;
    private boolean hasExpired = false;
    private double totalTimeSlice = 0.0;
    private long timeSliceCounter = 0L;

    private boolean firstTime = true;

    PerformanceScheduler(){

        maxWaitQueue = new PriorityQueue[2];
        for (int i = 0; i < 2; i++) {
            maxWaitQueue[i] = new PriorityQueue<Pcb>(20, new Comparator<Pcb>() {
                @Override
                public int compare(Pcb p1, Pcb p2) {

                    return (int) (p1.getPcbData().getWaitingSince() - p2.getPcbData().getWaitingSince());//LOOPHOLE?
                }
            });
        }
    }

    @Override
    public Pcb get(int cpuId) {
        //CHECK THE CPUID?

        if(firstTime)
            return null;

        boolean emptyPool = true;
        for (int i = 0; i < numCpus; i++)
            for (int j = 0; j < numPrio; j++)
                if (pool[poolInUse][i][j].size() > 0) {
                    emptyPool = false;
                    break;
                }

        if (emptyPool) {
            int tmp = poolInUse;
            poolInUse = poolExpired;
            poolExpired = tmp;
            hasExpired = false;
        }

        if (!hasExpired) {
            Pcb maxWaiting = maxWaitQueue[poolInUse].peek();

            if (maxWaiting == null)
                return null;//ERROR?
            else {
                //System.out.println(totalTimeSlice / timeSliceCounter);
                if (Process.getCurrentTime() - maxWaiting.getPcbData().getWaitingSince()
                        >= maxWaitQueue[poolInUse].size() * 13 * totalTimeSlice / timeSliceCounter) {//EDITABLE FACTOR
                    hasExpired = true;
                    System.out.println("\n\n\n\n\nEXPIRED\n\n\n\n\n");
                }
            }

        }

        int bestCpu = cpuId;
        int bestPrio = 0;
        Pcb toRet = null;

        for (int i = 0; i < numPrio; i++) {
            toRet = pool[poolInUse][cpuId][i].peek();

            if(toRet != null){
                bestPrio = i;
                break;
            }
        }

        if(toRet == null) {
            bestCpu = -1;
            bestPrio = -1;
            for(int i = 0; i < numCpus; i++){
                for(int j = 0; j < numPrio; j++) {
                    if (pool[poolInUse][i][j].peek() != null && (bestCpu == -1 || bestPrio == -1 ||
                            pool[poolInUse][bestCpu][bestPrio].peek().getPcbData().getBurstExecutionTime() <
                                    pool[poolInUse][i][j].peek().getPcbData().getBurstExecutionTime())) {
                        bestCpu = i;
                        bestPrio = j;
                        break;//Do not ascend down the priorities! - Vertical search; j and i placements are interchanged
                    }
                }
                /*if(bestCpu != -1)//horizontal search
                    break;*/
            }
            if(bestCpu == -1)
                toRet = null;
            else
                toRet = pool[poolInUse][bestCpu][bestPrio].poll();
        }

        if(toRet != null){
            long numOfProc = Process.getProcessCount();
            if (numOfProc == 0)
                numOfProc++;

            /*long newTimeSlice = (Process.getCurrentTime() - toRet.getPcbData().getWaitingSince()) / numOfProc;
            if(newTimeSlice == 0)
                newTimeSlice = 1;
            newTimeSlice *= 9;*/
            long newTimeSlice = (Process.getCurrentTime() - toRet.getPcbData().getWaitingSince()) / numOfProc;
            if(newTimeSlice == 0)
                newTimeSlice = 13;

            totalTimeSlice += newTimeSlice; timeSliceCounter++;//Aging statistics
            toRet.setTimeslice(newTimeSlice);
            pool[poolInUse][bestCpu][bestPrio].remove(toRet);
            maxWaitQueue[poolInUse].remove(toRet);
        }

        /*if(Process.getProcessCount() < 12){
            for(int i = 0; i < numPrio; i++) {
                for (int j = 0; j < numCpus; j++)
                    System.out.print(initialStatistics[i][j] + "  ");
                System.out.println(" ");
            }
        }*/

        return toRet;
    }

    @Override
    public void put(Pcb pcb) {

        if(firstTime){
            firstTime = false;
            numCpus = Pcb.RUNNING.length;

            pool = new PriorityQueue[2][numCpus][numPrio];
            for(int i = 0; i < 2; i++) {
                pool[i] = new PriorityQueue[numCpus][numPrio];
                for(int j = 0; j < numCpus; j++) {
                    for(int k = 0; k < numPrio; k++) {
                        pool[i][j][k] = new PriorityQueue<Pcb>(new Comparator<Pcb>() {
                            @Override
                            public int compare(Pcb p1, Pcb p2) {

                                return (int) (p1.getPcbData().getBurstExecutionTime() - p2.getPcbData().getBurstExecutionTime());//LOOPHOLE?
                            }
                        });
                    }
                }
            }

            initialStatistics = new long[numPrio][numCpus];
            for(int i = 0; i < numPrio; i++){
                initialStatistics[i] = new long[numCpus];
            }
        }

        if(pcb == null || pcb.getPreviousState() == Pcb.ProcessState.IDLE)
            return;

        int targetQueue = pcb.getAffinity();
        int targetPriority = pcb.getPriority();

        if(targetPriority < 0)
            targetPriority = 0;
        if(targetPriority >= numPrio)
            targetPriority = numPrio - 1;

        //Improved load balancing
        int minLoadCpuIndex = 0;
        for(int i = 1; i < numCpus; i++){
            if(initialStatistics[targetPriority][i] < initialStatistics[targetPriority][minLoadCpuIndex])
                minLoadCpuIndex = i;
        }

        if(pcb.getPreviousState() == Pcb.ProcessState.CREATED || pcb.getPreviousState() == Pcb.ProcessState.BLOCKED){

            if(pcb.getPreviousState() == Pcb.ProcessState.CREATED) {
                pcb.setPcbData(new PcbData());
                //pcb.setAffinity(initialLoadBalancing);
                targetQueue = minLoadCpuIndex;
                ++initialStatistics[targetPriority][minLoadCpuIndex];//Improved load balancing
                //initialLoadBalancing = ++initialLoadBalancing % numCpus;
            }

            pcb.getPcbData().setBurstExecutionTime(0L);
        }
        else
            pcb.getPcbData().setBurstExecutionTime(pcb.getPcbData().getBurstExecutionTime() + pcb.getExecutionTime());

        pcb.getPcbData().setWaitingSince(Process.getCurrentTime());

        if(!hasExpired) {
            pool[poolInUse][targetQueue][targetPriority].add(pcb);
            maxWaitQueue[poolInUse].add(pcb);
        }
        else {
            pool[poolExpired][targetQueue][targetPriority].add(pcb);
            maxWaitQueue[poolExpired].add(pcb);
        }
    }
}
