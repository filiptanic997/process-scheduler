package com.etf.os2.project.scheduler;

import com.etf.os2.project.process.Pcb;
import com.etf.os2.project.process.PcbData;
import com.etf.os2.project.process.Process;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by Filip Tanic on 5.1.2019..
 */
public class CFScheduler extends Scheduler {

    private PriorityQueue<Pcb> maxWaitQueue[];
    private PriorityQueue<Pcb> pool[];
    private int poolInUse = 0;
    private int poolExpired = 1;
    private boolean hasExpired = false;
    private double totalTimeSlice = 0.0;
    private long timeSliceCounter = 0L;

    CFScheduler(){
        pool = new PriorityQueue[2];
        for(int i = 0; i < 2; i++){
            pool[i] = new PriorityQueue<Pcb>(20, new Comparator<Pcb>() {
                @Override
                public int compare(Pcb p1, Pcb p2) {

                    return (int)(p1.getPcbData().getBurstExecutionTime() - p2.getPcbData().getBurstExecutionTime());//LOOPHOLE?
                }
            });
        }
        maxWaitQueue = new PriorityQueue[2];
        for(int i = 0; i < 2; i++){
            maxWaitQueue[i] = new PriorityQueue<Pcb>(20, new Comparator<Pcb>() {
                @Override
                public int compare(Pcb p1, Pcb p2) {

                    return (int)(p1.getPcbData().getWaitingSince() - p2.getPcbData().getWaitingSince());//LOOPHOLE?
                }
            });
        }
    }

    @Override
    public Pcb get(int cpuId) {
        //CHECK THE CPUID?
        if(pool[poolInUse].size() == 0){
            int tmp = poolInUse; poolInUse = poolExpired; poolExpired = tmp;
            hasExpired = false;
        }

        if(!hasExpired){
            Pcb maxWaiting = maxWaitQueue[poolInUse].peek();

            if(maxWaiting == null)
                return null;//ERROR?
            else{
                System.out.println(totalTimeSlice / timeSliceCounter);
                if(Process.getCurrentTime() - maxWaiting.getPcbData().getWaitingSince()
                        >=  maxWaitQueue[poolInUse].size() * totalTimeSlice / timeSliceCounter) {
                    hasExpired = true;
                    System.out.println("\n\n\n\n\nEXPIRED\n\n\n\n\n");
                }
            }

        }

        Pcb toRet = pool[poolInUse].poll();

        if(toRet != null){

            long numOfProc = Process.getProcessCount();
            if (numOfProc == 0)
                numOfProc++;

            long newTimeSlice = (Process.getCurrentTime() - toRet.getPcbData().getWaitingSince()) / numOfProc;
            if(newTimeSlice == 0)
                newTimeSlice = 13;

            totalTimeSlice += newTimeSlice; timeSliceCounter++;//Aging statistics
            toRet.setTimeslice(newTimeSlice);
            pool[poolInUse].remove(toRet);
            maxWaitQueue[poolInUse].remove(toRet);
        }

        return toRet;
    }

    @Override
    public void put(Pcb pcb) {

        if(pcb == null || pcb.getPreviousState() == Pcb.ProcessState.IDLE)
            return;

        if(pcb.getPreviousState() == Pcb.ProcessState.CREATED || pcb.getPreviousState() == Pcb.ProcessState.BLOCKED){

            if(pcb.getPreviousState() == Pcb.ProcessState.CREATED)
                pcb.setPcbData(new PcbData());

            pcb.getPcbData().setBurstExecutionTime(0L);
        }
        else
            pcb.getPcbData().setBurstExecutionTime(pcb.getPcbData().getBurstExecutionTime() + pcb.getExecutionTime());

        pcb.getPcbData().setWaitingSince(Process.getCurrentTime());

        if(!hasExpired) {
            pool[poolInUse].add(pcb);
            maxWaitQueue[poolInUse].add(pcb);
        }
        else {
            pool[poolExpired].add(pcb);
            maxWaitQueue[poolExpired].add(pcb);
        }
    }
}
