package com.etf.os2.project.scheduler;

import com.etf.os2.project.process.Pcb;

import java.util.ArrayList;

public abstract class Scheduler {
    public abstract Pcb get(int cpuId);

    public abstract void put(Pcb pcb);

    public static Scheduler createScheduler(String[] args) {

		Scheduler toRet = null;

        if(args.length == 0)
            toRet = null;
		else if(args[0].equals("SJF"))
            toRet = new SJFScheduler(Boolean.parseBoolean(args[1]), Double.parseDouble(args[2]));
		else if(args[0].equals("MFQ")) {
		    ArrayList<Long> timeSlices = new ArrayList();
		    for(int i = 0; i < args.length - 1; i++)
		        timeSlices.add(timeSlices.size(), Long.parseLong(args[i + 1]));
            toRet = new MFQScheduler(timeSlices);
        }
        else if(args[0].equals("CF"))
		    toRet = new CFScheduler();
		else if(args[0].equals("PF"))
            toRet = new PerformanceScheduler(/*Boolean.parseBoolean(args[1]), Double.parseDouble(args[2])*/);
		else{
		    System.out.println("Scheduler TAG not found!");
            System.out.println("Possible TAGs are: SJF, MFQ, CF and PF");
        }

        return toRet;
    }
}
