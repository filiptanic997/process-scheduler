package com.etf.os2.project.scheduler;

import com.etf.os2.project.Main;
import com.etf.os2.project.process.Pcb;
import com.etf.os2.project.process.PcbData;
import com.etf.os2.project.process.Process;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by Filip Tanic on 30.12.2018..
 */
public class SJFScheduler extends Scheduler {

    private static int numCpus;

    private PriorityQueue<Pcb>[][] queues;
    private boolean isPreemptive;
    private double meanFactor;
    //EXPIRATION MECHANISM
    private int poolInUse = 0;
    private int poolExpired = 1;
    private boolean hasExpired = false;
    private PriorityQueue<Pcb> longestQueue[];

    private boolean firstTime = true;

    SJFScheduler(boolean isPreemptive, double meanFactor){/**meanFactor must be in range[0, 1]!*/

        this.isPreemptive = isPreemptive;
        this.meanFactor = meanFactor;

        longestQueue = new PriorityQueue[2];
        for(int i = 0; i < 2; i++){
            longestQueue[i] = new PriorityQueue<Pcb>(32, new Comparator<Pcb>() {
                @Override
                public int compare(Pcb p1, Pcb p2) {
                    if (p2.getPcbData().getOldPrediction() < p1.getPcbData().getOldPrediction())
                        return -1;
                    else if (p2.getPcbData().getOldPrediction() == p1.getPcbData().getOldPrediction())
                        return 0;
                    else
                        return 1;
                }
            });
        }
    }

    @Override
    public Pcb get(int cpuId) {

        if(firstTime)
            return null;

        if(cpuId < 0 || cpuId >= numCpus)
            return null;

        boolean emptyPool = true;
        for (int i = 0; i < numCpus; i++) {
            if (queues[poolInUse][i].size() > 0) {
                emptyPool = false;
                break;
            }
        }

        if (emptyPool) {
            int tmp = poolInUse; poolInUse = poolExpired; poolExpired = tmp; hasExpired = false;
        }

        if(!hasExpired){
            Pcb maxWaiting = longestQueue[poolInUse].peek();

            if(maxWaiting == null)
                return null;//ERROR?
            else{
                if(Process.getCurrentTime() - maxWaiting.getPcbData().getWaitingSince()
                        >=  longestQueue[poolInUse].size() * 33) {
                    hasExpired = true;
                    System.out.println("\n\n\n\n\nEXPIRED\n\n\n\n\n");
                }
            }

        }

        double[] predictions = new double[numCpus];
        for(int i = 0; i < numCpus; i++)
            predictions[i] = -1;

        int bestIndex = -1;

        for(int i = 0; i < numCpus; i++){
            Pcb curr = queues[poolInUse][i].peek();

            if(curr != null){
                double currPred = curr.getPcbData().getOldPrediction();
                predictions[i] = currPred;

                if(bestIndex < 0 || currPred < predictions[bestIndex]){//LOOPHOLE?!
                    bestIndex = i;
                }
            }
        }

        if(bestIndex < 0)
            return null;

        Pcb toRet = null;

        if (bestIndex == cpuId)
            toRet = queues[poolInUse][bestIndex].poll();
        else if (predictions[cpuId] != -1 && predictions[cpuId] < predictions[bestIndex] * 1000.0) {//EDITABLE FACTOR!
            /*Main.pickedCPUOverBest++;
            System.out.println("prediction[best]: " + predictions[bestIndex]);
            System.out.println("prediction[cpuId]: " + predictions[cpuId]);*/
            toRet = queues[poolInUse][cpuId].poll();
        }
        else {
            //Main.pickedBestOverCPU++;
            toRet = queues[poolInUse][bestIndex].poll();
        }

        toRet.getPcbData().setCurrentStartTime(Process.getCurrentTime());
        longestQueue[poolInUse].remove(toRet);

        //Main.totalSwitched++;

        return toRet;

    }

    @Override
    public void put(Pcb pcb) {

        if(firstTime){
            firstTime = false;
            numCpus = Pcb.RUNNING.length;

            queues = new PriorityQueue[2][numCpus];
            for(int j = 0; j < 2; j++) {
                for (int i = 0; i < numCpus; i++) {

                    queues[j][i] = new PriorityQueue<Pcb>(32, new Comparator<Pcb>() {

                        @Override
                        public int compare(Pcb p1, Pcb p2) {
                            if (p1.getPcbData().getOldPrediction() < p2.getPcbData().getOldPrediction())
                                return -1;
                            else if (p1.getPcbData().getOldPrediction() == p2.getPcbData().getOldPrediction())
                                return 0;
                            else
                                return 1;
                        }
                    });
                }
            }
        }

        if(pcb == null || pcb.getPreviousState() == Pcb.ProcessState.IDLE)
            return;

        int targetQueue = pcb.getAffinity();

        if(pcb.getPreviousState() == Pcb.ProcessState.CREATED) {
            pcb.setPcbData(new PcbData());
            //LOAD BALANCING LOGIC - insufficient here!
            /*long minProc = Long.MAX_VALUE;
            for(int i = 0; i < numCpus; i++){
                if(minProc > queues[i].size()){
                    targetQueue = i;
                    minProc = queues[i].size();
                }
            }*/
        }

        double newPrediction = Double.MAX_VALUE;

        if(pcb.getPcbData().isHasBeenPreempted()){
            newPrediction = pcb.getPcbData().getOldPrediction() - pcb.getExecutionTime();
        }
        if(!pcb.getPcbData().isHasBeenPreempted() || newPrediction <= 0.0) {
            pcb.getPcbData().setHasBeenPreempted(false);
            double oldPrediction = pcb.getPcbData().getOldPrediction();
            newPrediction = meanFactor * pcb.getExecutionTime() + (1 - meanFactor) * oldPrediction;
        }

        pcb.getPcbData().updatePrediction(newPrediction);

        if(isPreemptive){//PREEMPTION LOGIC
            int maxIndex = -1;
            double max = newPrediction;

            for(int i = 0; i < Pcb.RUNNING.length ; i++){

                if(Pcb.RUNNING[i] == null) {
                    maxIndex = i;
                    break;
                }

                PcbData targetData = Pcb.RUNNING[i].getPcbData();

                if(targetData == null)
                    continue;

                double leftToExecute = targetData.getOldPrediction() + targetData.getCurrentStartTime() - Process.getCurrentTime();

                if(max < leftToExecute && Pcb.RUNNING[i].getPreviousState() != Pcb.ProcessState.IDLE){
                    max = leftToExecute;
                    maxIndex = i;
                }
            }

            if(maxIndex >= 0){
                Pcb.RUNNING[maxIndex].setPreempt(true);
                Pcb.RUNNING[maxIndex].getPcbData().setHasBeenPreempted(true);
            }
        }

        pcb.getPcbData().setWaitingSince(Process.getCurrentTime());

        if(!hasExpired) {
            queues[poolInUse][targetQueue].add(pcb);
            longestQueue[poolInUse].add(pcb);
        }
        else{
            queues[poolExpired][targetQueue].add(pcb);
            longestQueue[poolExpired].add(pcb);
        }
    }

}
