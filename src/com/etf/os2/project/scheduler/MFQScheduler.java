package com.etf.os2.project.scheduler;

import com.etf.os2.project.process.Pcb;
import com.etf.os2.project.process.PcbData;
import com.etf.os2.project.process.Process;

import javax.lang.model.type.ArrayType;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by Filip Tanic on 4.1.2019..
 */
public class MFQScheduler extends Scheduler {

    private int numOfQueues;
    ArrayList<Long> queueTimeSlices;
    Queue<Pcb>[] queues;

    MFQScheduler(ArrayList<Long> queueTimeSlices){

        this.numOfQueues = queueTimeSlices.size();
        this.queueTimeSlices = queueTimeSlices;
        this.queues = new Queue[queueTimeSlices.size()];

        for(int i = 0; i < numOfQueues; i++)
            queues[i] = new LinkedList();
    }

    @Override
    public Pcb get(int cpuId) {        //CHECK THE CPUID?
        age(150);

        for(int i = 0; i < numOfQueues; i++){
            if(queues[i].size() > 0) {
                //System.out.println(queues[i].peek().getTimeslice());
                Pcb toRet;
                Object[] toRetQueue = queues[i].stream().filter(p -> p.getAffinity() == cpuId).toArray();
                if(toRetQueue.length > 0){
                    toRet = (Pcb) toRetQueue[0];
                    queues[i].remove(toRet);
                    return toRet;
                }
                return queues[i].poll();
            }
        }

        return null;
    }

    @Override
    public void put(Pcb pcb) {

        if(pcb == null || pcb.getPreviousState() == Pcb.ProcessState.IDLE)
            return;

        if(pcb.getPreviousState() == Pcb.ProcessState.CREATED) {
            pcb.setPcbData(new PcbData());
            pcb.getPcbData().setMfqPriority(pcb.getPriority());
        }

        int targetIndex = pcb.getPcbData().getMfqPriority();

        if(pcb.getPreviousState() == Pcb.ProcessState.BLOCKED)
            targetIndex--;
        else if(pcb.getPreviousState() == Pcb.ProcessState.RUNNING)
            targetIndex++;

        if(targetIndex < 0)
            targetIndex = 0;
        if(targetIndex >= numOfQueues)
            targetIndex = numOfQueues - 1;

        pcb.getPcbData().setMfqPriority(targetIndex);
        pcb.setTimeslice(queueTimeSlices.get(targetIndex));
        queues[targetIndex].add(pcb);
        pcb.getPcbData().setWaitingSince(Process.getCurrentTime());
    }

    private void age(long agingLimit){

        if(agingLimit <= 0L)
            agingLimit = 1L;

        for(int i = 0; i < numOfQueues; i++){

            int queueSize = queues[i].size();

            for(int j = 0; j < queueSize; j++){
                Pcb curr = queues[i].poll();

                long waitingFor = Process.getCurrentTime() - curr.getPcbData().getWaitingSince();

                if(waitingFor >= agingLimit){
                    int targetQueue = i - 1;
                    if(targetQueue < 0)
                        ++targetQueue;

                    System.out.println("\n\n\nPID" + curr.getId() + " AGED!\n\n\n");

                    curr.getPcbData().setWaitingSince(Process.getCurrentTime() - (waitingFor - agingLimit));
                    queues[targetQueue].add(curr);
                    curr.setTimeslice(queueTimeSlices.get(targetQueue));
                }
                else {
                    queues[i].add(curr);
                    curr.setTimeslice(queueTimeSlices.get(i));
                }
            }
        }
    }

}
