package com.etf.os2.project;


import com.etf.os2.project.process.Process;
import com.etf.os2.project.process.RandomGenerator;
import com.etf.os2.project.scheduler.PerformanceScheduler;
import com.etf.os2.project.scheduler.SJFScheduler;
import com.etf.os2.project.scheduler.Scheduler;
import com.etf.os2.project.system.System;
import sun.misc.Perf;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    //My change!
    //TOTAL statistics
    public static double totalResponseTime = 0.0;
    public static double totalExecutionTime = 0.0;
    //SJF statistics
    //public static long totalSwitched = 0L;
    //public static long pickedCPUOverBest = 0L;
    //public static long pickedBestOverCPU = 0L;
    //My change!

    public static void main(String[] args) {
        if (args.length < 2) {
            java.lang.System.err.println("Invalid arguments\n\nUsage:\n" +
                    "\t<NUM_CPUS> <NUM_PROCESSES> [<scheduler_args>]");
            java.lang.System.exit(-1);
        }

        int numCpus = Integer.parseInt(args[0]);
        int numProcesses = Integer.parseInt(args[1]);

        int processType = 0, processLength = 0;
        List<Process> processes = new ArrayList<Process>();
        for (int i = 1; i <= numProcesses; i++) {
            Process process = new Process(i, i % 5,
                    Process.ProcessType.values()[processType],
                    Process.ProcessLength.values()[processLength]);
            processes.add(process);

            processType = (processType + 1) % Process.ProcessType.values().length;
            processLength = (processLength + 1) % Process.ProcessLength.values().length;
        }

        String[] schedArgs = new String[args.length - 2];
        java.lang.System.arraycopy(args, 2, schedArgs, 0, schedArgs.length);
        //My change!
        //SJFScheduler.setNumCpus(numCpus);
        //PerformanceScheduler.setNumCpus(numCpus);
        //My change!
        Scheduler scheduler = Scheduler.createScheduler(schedArgs);//My Change!
        System system = new System(scheduler, numCpus, processes);

        system.work();

        //My change!
        java.lang.System.out.println("STATS:");
        java.lang.System.out.println("\tAverage ExecutionTime: " + totalExecutionTime / numProcesses);
        java.lang.System.out.println("\tAverage ResponseTime: " + totalResponseTime / numProcesses);
        /*java.lang.System.out.println("\tSJF Total switches: " + totalSwitched);
        java.lang.System.out.println("\tSJF picked best over cpuID: " + pickedBestOverCPU);
        java.lang.System.out.println("\tSJF picked cpuId over best: " + pickedCPUOverBest);*/
        //My change!
    }
}
