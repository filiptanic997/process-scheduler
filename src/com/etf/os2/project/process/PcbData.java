package com.etf.os2.project.process;

public class PcbData {

    //GENERAL
    private long waitingSince = 0;


    //SJF
    private double oldPrediction = 1.0;
    private long currentStartTime = 0;
    private boolean hasBeenPreempted = false;
    //MFQ
    private int mfqPriority = -1;
    //CF
    private long burstExecutionTime = 0;

    public double getOldPrediction() {
        return oldPrediction;
    }

    public void updatePrediction(double newPrediction) {
        this.oldPrediction = newPrediction;
    }

    public long getCurrentStartTime() {
        return currentStartTime;
    }

    public void setCurrentStartTime(long currentStartTime) {
        this.currentStartTime = currentStartTime;
    }

    public int getMfqPriority() {
        return mfqPriority;
    }

    public void setMfqPriority(int mfqPriority) {
        this.mfqPriority = mfqPriority;
    }

    public long getBurstExecutionTime() {
        return burstExecutionTime;
    }

    public void setBurstExecutionTime(long burstExecutionTime) {
        this.burstExecutionTime = burstExecutionTime;
    }

    public long getWaitingSince() {
        return waitingSince;
    }

    public void setWaitingSince(long waitingSince) {
        this.waitingSince = waitingSince;
    }

    public boolean isHasBeenPreempted() {
        return hasBeenPreempted;
    }

    public void setHasBeenPreempted(boolean hasBeenPreempted) {
        this.hasBeenPreempted = hasBeenPreempted;
    }
}
